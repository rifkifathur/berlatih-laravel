<?php

	Route::get('/index', function (){
		return view('index');
	});

	Route::get('/', 'HomeController@home');
	Route::get('/register', 'AuthController@register');
	Route::post('/welcome', 'AuthController@welcome');

?>
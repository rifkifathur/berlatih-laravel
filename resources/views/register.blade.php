<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SanberBook</title>
	</head>
	<body>
		<div>
			<h2>Buat Account Baru!</h2>
		</div>

		<div>
			<h4>Sign Up Form</h4>
			<form action="welcome" method="POST">
				@csrf
				<!-- Name -->
				<p><label>Fist Name:</label></p>
				<input type="text" name="NamaDepan">
				<p><label>Last Name:</label></p>
				<input type="text" name="NamaBelakang">

				<!-- Gender -->
				<p><label>Gender:</label></p>
				<input type="radio" name="gender" value="0">Male
				<br>
				<input type="radio" name="gender" value="1">Female
				<br>
				<input type="radio" name="gender" value="2">Other

				<!-- Country -->
				<p><label>Nationality:</label></p>
				<select>
					<option value="0">Indonesian</option>
					<option value="1">English</option>
					<option value="2">Chiness</option>
					<option value="3">Germany</option>
					<option value="4">Spain</option>
					<option value="5">Philpin</option>
				</select>

				<!-- Language -->
				<p><label>Lanuage Spoken:</label></p>
				<input type="checkbox" name="language" value="0">Bahasa Indonesia
				<br>
				<input type="checkbox" name="language" value="1">English
				<br>
				<input type="checkbox" name="language" value="2">Other

				<!-- Bio -->
				<p><label for="Bio">Bio:</label></p>
				<textarea cols="27" rows="7"></textarea>
				<br>
				<input type="submit" value="Sign Up">
			</form>
		</div>
	</body>
</html>